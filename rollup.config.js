import { join } from 'path';
import url from 'postcss-url';
import csso from 'csso';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import resolve from 'rollup-plugin-node-resolve';
import postcss from 'postcss';
import scss from 'rollup-plugin-scss';
import postcssModulesLocalByDefault from 'postcss-modules-local-by-default';
import postcssModulesScope from 'postcss-modules-scope';
import { terser } from 'rollup-plugin-terser';
import html from '@rollup/plugin-html';
import fs from 'fs-extra';
import { mapToAttrs } from './src/utils/inject';

const production = !process.env.ROLLUP_WATCH;
const extensions = ['.ts', '.tsx', '.js'];

const modules = fs.readdirSync(join(__dirname, './src/modules')).filter((i) => !i.startsWith('.'));
const distDir = join(__dirname, 'dist');

export default modules.map(createConfig).flat();

function createConfig(name) {
  const plugins = [resolve({ extensions, preferBuiltins: true }), commonjs(), production && terser()];
  const moduleDir = join(__dirname, `src/modules/${name}`);
  const generateScopedName = production ? alphabetId() : (i) => '_' + i;
  const xxx = postcss([
    url({
      url: 'inline',
      maxSize: 100,
      filter: /\.(woff2?|svg)$/,
    }),
    postcssModulesLocalByDefault(),
    postcssModulesScope({
      generateScopedName,
    }),
  ]);

  return [
    {
      input: moduleDir,
      output: {
        sourcemap: false,
        format: 'cjs',
        file: join(distDir, `${name}.js`),
      },
      plugins: [...plugins, hexoGeneratorInjector()],
      external: ['crypto', 'fs', 'path'],
    },
    {
      input: join(moduleDir, 'client'),
      output: {
        sourcemap: !production,
        format: 'iife',
        dir: distDir,
        entryFileNames: `${name}.[hash].js`,
      },
      plugins: [
        ...plugins,
        !production &&
          html({
            fileName: `${name}.html`,
            publicPath: '',
            template: ({ files }) => {
              return [
                '<!DOCTYPE html><html><head><meta charset="UTF-8">',
                '<meta name="viewport" content="width=device-width, initial-scale=1.0">',
                `<title>${name}</title>`,
                '<link rel="stylesheet" href="../public/style.css">',
                '<script src="../public/config.js"></script>',
                `<script src="${files.js[0].fileName}"></script>`,
                `</head><body><main>${renderSample(name)}</main></body></html>`,
              ].join('');
            },
          }),
        scss({
          output: false,
          processor: (raw) => {
            const [code, vars] = xxx.process(raw).css.split(':export');
            const css = !production ? code : csso.minify(code).css;
            return JSON.stringify([
              css,
              new Function('return ' + vars.replace(/([^\s]+):\s*([^\s]+);/g, '"$1": "$2",'))(),
            ]);
          },
        }),
        babel({
          extensions,
          presets: ['@babel/typescript', '@babel/env'],
          plugins: [
            ['@babel/plugin-proposal-decorators', { legacy: true }],
            ['@babel/plugin-proposal-class-properties', { loose: true }],
            [
              '@babel/plugin-transform-react-jsx',
              {
                pragma: 'h',
                pragmaFrag: 'Fragment',
              },
            ],
          ],
        }),
        !production && livereload('public'),
      ],
    },
  ].filter((i) => i);
}

function renderSample(name) {
  const { children = '', ...props } = require(join(__dirname, `./src/modules/${name}/sample.json`));
  return (
    `<is-${name}${mapToAttrs(props)}>${children}</is-${name}>`
  );
}

const generator_code = `
hexo.extend.generator.register('REPLACEMENT_NAME', () => {
  return { path: 'REPLACEMENT_NAME', data: helper.i18n('REPLACEMENT_CODE') };
});
hexo.extend.injector.register('head_end', helper.js('REPLACEMENT_NAME'));
`;

function hexoGeneratorInjector() {
  return {
    name: 'hexo-generator-injector',
    transform(code, id) {
      if (/src\/modules\/[a-z\-]+\/index.js$/.test(id)) {
        code = `${code};export const filename = 'REPLACEMENT_NAME';`;

        return code.replace('/* replacement generator */', generator_code);
      }
      return null;
    },
  };
}

function alphabetId() {
  const cache = {};
  let i = 0;
  return function (token) {
    if (cache[token]) return cache[token];
    cache[token] = alphabet(i++);
    return cache[token];
  };
}

function alphabet(i) {
  if (i < 26) return String.fromCharCode(97 + i);

  return alphabet(~~(i / 26)) + alphabet(i % 26);
}
