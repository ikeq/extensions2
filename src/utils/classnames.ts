export function classnames(...args) {
  return args.reduce((ret, i) => {
    if (i) ret.push(i);
    return ret;
  }, []).join(' ');
}
