interface Context {
  root: ShadowRoot;
  [key: string]: any;
}
interface Component {
  style?: string;
  attrs?: string[];
  shared?: (context: Context) => void;
  created?: (root: ShadowRoot) => any;
  connected?: (context: Context) => void;
  changed?: (context: Context, changed: [string, any, any]) => void;
}

export function define(name: string, options: Component) {
  const context: Context = { root: null };
  if (options.shared) options.shared(context);
  customElements.define(
    `is-${name}`,
    class extends HTMLElement {
      static get observedAttributes() { return options.attrs; }
      constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        const style = document.createElement('style');
        style.textContent = options.style;
        if (options.created) {
          Object.assign(context, options.created(shadowRoot));
        }
        context.root = shadowRoot;
        if (options.style) {
          shadowRoot.appendChild(style);
        }
      }
      connectedCallback() {
        if (options.connected) options.connected(context);
      }
      attributeChangedCallback(name, oldValue, newValue) {
        if (options.changed) options.changed(context, [name, oldValue, newValue]);
      }
    }
  );
}
