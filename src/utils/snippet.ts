// https://bitbucket.org/ikeq/hexo-theme-inside-ng/src/master/src/app/directives/snippet.directive.ts

const GIST_REGEX = /^https?\:\/\/gist.github.com/;

export function execHtml(html: string, replacedNode: HTMLElement) {
  const div = document.createElement('div');
  div.innerHTML = html;
  exec(div, 16.667);
  replacedNode.insertAdjacentElement('beforebegin', div);
  replacedNode.parentNode.removeChild(replacedNode);
}

export function exec(node: HTMLElement, delay?: number) {
  setTimeout(() => {
    const list = node.getElementsByTagName('script');

    if (list && list.length) {
      Array.from(list).forEach((script: HTMLScriptElement) => {
        const parent = document.createElement('div');
        script.parentElement.insertBefore(parent, script);

        const { innerHTML: scriptCode, src: scriptSrc } = script;
        if (!scriptCode && !scriptSrc) return;

        // gist
        if (scriptSrc && scriptSrc.match(GIST_REGEX)) {
          let iframe = document.createElement('iframe');
          iframe.style.display = 'none';
          iframe.onload = () => {
            let iframeDocument = iframe.contentDocument;
            if (!iframeDocument) return;

            const gist = document.createElement('div');
            const gistStyle = iframeDocument.querySelector('link[rel="stylesheet"]');
            const gistContent = iframeDocument.querySelector('.gist');

            if (gistStyle && gistContent) {
              const html = gistContent.cloneNode() as HTMLElement;
              html.innerHTML = gistContent.innerHTML;
              gist.appendChild(gistStyle.cloneNode());
              gist.appendChild(html);
            }
            parent.removeChild(iframe);
            parent.appendChild(gist);
            iframe = iframeDocument = null;
          };
          iframe.srcdoc = `<script src="${scriptSrc}"></script>`;
          parent.appendChild(iframe);
          return;
        }

        // other script
        else {
          const runScript = document.createElement('script');
          if (scriptSrc) {
            runScript.src = scriptSrc;
          } else {
            runScript.innerHTML = scriptCode;
          }
          parent.appendChild(runScript);
        }

        // XD
        parent.parentElement.removeChild(parent);
      });
    }
  }, delay);
}
