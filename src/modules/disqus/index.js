import { mapToAttrs } from '../../utils/inject';

export function exec(hexo, config = {}, helper) {
  const disqus = { ...config };
  disqus.script = disqus.script || `//${disqus.shortname}.disqus.com/embed.js`;
  delete disqus.shortname;

  /* replacement generator */

  return [
    {
      position: 'comments',
      template: `<is-disqus${mapToAttrs(disqus)}></is-disqus>`,
    },
  ];
}

export const schema = {
  type: 'object',
  properties: {
    shortname: { type: 'string' },
    script: { type: 'string', format: 'uri' },
    autoload: { type: 'boolean', default: true },
  },
  required: ['shortname', 'autoload'],
  additionalProperties: false,
};
