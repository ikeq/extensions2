import { createElement, define } from '../../utils';

import raw from './style.scss';
const [style, css] = JSON.parse(raw);

define('disqus', {
  style,
  attrs: ['autoload', 'script'],
  shared(context) {
    document.addEventListener('inside', ({ detail }: any) => {
      if (detail.type !== 'route') return;
      context.current = {
        title: detail.data.title,
        url: detail.data.url,
        identifier: detail.data.id
      };
    })
  },
  connected(context) {
    const config = {
      autoload: context.root.host.hasAttribute('autoload'),
      script: context.root.host.getAttribute('script')
    };
    const disqusHtml = `<div id="disqus_thread"></div><div id="disqus_recommendations"></div>`;

    if (window.DISQUS || config.autoload) return load();

    const loading = createElement('div', { class: css.loading, props: { innerHTML: 'Loading...' } });
    const button = createElement('a', { class: css.button, props: { textContent: '{{ comments.load | Disqus }}' } });
    button.addEventListener('click', load);
    context.root.appendChild(button);

    function load() {
      if (window.DISQUS) {
        return reset();
      }
      window.disqus_config = function () {
        Object.assign(this.page, context.current);
      }
      context.root.host.innerHTML = disqusHtml;
      context.root.removeChild(button);
      context.root.appendChild(loading);
      loadScript(config.script).then(reset).catch(() => {
        button.textContent = '{{ comments.load_faild | Disqus }}';
        context.root.appendChild(button);
      }).finally(() => {
        context.root.removeChild(loading);
      });

      function reset() {
        context.root.host.outerHTML = disqusHtml;
        setTimeout(() => {
          window.DISQUS.reset({
            reload: true,
            config() {
              Object.assign(this.page, context.current);
            }
          });
        }, 0);
      }
    }
  }
});

function loadScript(src) {
  const s = document.createElement('script');
  s.src = 'https:' + src;

  return new Promise((resolve, reject) => {
    s.onload = resolve;
    s.onerror = (e) => {
      document.body.removeChild(s);
      reject(e);
    };
    document.body.appendChild(s);
  });
}
