import { mapToAttrs } from '../../utils/inject';

export function exec(hexo, config = {}, helper) {
  /* replacement generator */

  return [
    {
      position: 'sidebar',
      template: `<is-palette${mapToAttrs(config)}></is-palette>`,
    },
  ];
}

export const schema = {
  type: 'object',
  properties: {
    theme: {
      type: 'array',
      items: { type: 'string', pattern: '^#(?:[0-9a-fA-F]{3}){1,2}$' },
    },
    col: { type: 'number', minimum: 1 },
  },
  required: ['theme'],
  additionalProperties: false,
};
