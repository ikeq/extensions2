import debounce from 'lodash/debounce';
import { createElement, define } from '../../utils';

import raw from './style.scss';
const [style, css] = JSON.parse(raw);

const STORAGE_KEY = '__theme_inside_darkmode__';

const theme = {
  light: {
    foreground_color: '#363636',
    border_color: '#e0e0e0',
    background: '#f3f6f7',
    // sidebar_background:
    //   '#607d8b linear-gradient(to bottom,#607d8b 0%,#26323b 100%)',
    card_background: '#fff',
    highlight: []
  },
  dark: {
    foreground_color: '#d8d8d8',
    border_color: '#444',
    background: '#202020',
    // sidebar_background: '#262626',
    card_background: '#252525',
    highlight: [
      '#252525', '#444', '#939393', '#585858',
      '#b8b8b8', '#d8d8d8', '#e8e8e8', '#f8f8f8',
      '#ab4642', '#dc9656', '#f7ca88', '#a1b56c',
      '#b8b8b8', '#7cafc2', '#ba8baf', '#a16946'
    ]
  }
};

define('darkmode', {
  style,
  attrs: ['mode'],
  created(root) {
    let dark = !!localStorage.getItem(STORAGE_KEY);
    const el = createElement('span', { class: css.root }, [
      createElement('span', { class: css.track }, [
        createElement('span', { class: css.thumb })
      ])
    ]);

    root.addEventListener('click', debounce(toggle, 100));

    toggle(true);
    root.appendChild(el);
    window.dispatchEvent(new Event('resize'));

    return { el };

    function toggle(init?: boolean) {
      if (typeof init !== 'boolean') init = undefined;
      if (!init) {
        dark = !dark;
        document.dispatchEvent(new CustomEvent('inside', {
          detail: {
            type: 'theme',
            data: theme[dark ? 'dark' : 'light']
          }
        }));
      }
      if (dark) {
        el.dataset.colorMode = 'dark';
        el.title = 'dark mode';
        localStorage.setItem(STORAGE_KEY, String(+dark));
      } else {
        el.removeAttribute('data-color-mode');
        el.title = 'light mode';
        localStorage.removeItem(STORAGE_KEY);
      }
    }
  }
});
